/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.triliapp.dto;

/**
 *
 * @author yasuly
 */
public class Rol {
    private Integer idRol;
    private String nombreRL;

    public Integer getIdRol() {
        return idRol;
    }

    public void setIdRol(Integer idRol) {
        this.idRol = idRol;
    }

    public String getNombreRL() {
        return nombreRL;
    }

    public void setNombreRL(String nombreRL) {
        this.nombreRL = nombreRL;
    }

    
}
