/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.triliapp.dto;
import java.sql.Timestamp;

/**
 *
 * @author Estudiante 2018
 */
public class CalificacionDeProducto {
    private Integer idCalificacionDeProducto;
    private float valorCPR;
    private Timestamp fechaCPR;
    private Usuario Usuario;
    private Producto Producto;

    public Integer getIdCalificacionDeProducto() {
        return idCalificacionDeProducto;
    }

    public void setIdCalificacionDeProducto(Integer idCalificacionDeProducto) {
        this.idCalificacionDeProducto = idCalificacionDeProducto;
    }

    public float getValorCPR() {
        return valorCPR;
    }

    public void setValorCPR(float valorCPR) {
        this.valorCPR = valorCPR;
    }

    public Timestamp getFechaCPR() {
        return fechaCPR;
    }

    public void setFechaCPR(Timestamp fechaCPR) {
        this.fechaCPR = fechaCPR;
    }

    public Usuario getUsuario() {
        return Usuario;
    }

    public void setUsuario(Usuario Usuario) {
        this.Usuario = Usuario;
    }

    public Producto getProducto() {
        return Producto;
    }

    public void setProducto(Producto Producto) {
        this.Producto = Producto;
    }
    
    
}
