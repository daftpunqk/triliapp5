package co.com.triliapp.dto;

public class Usuario {

    private Integer idUsuario;
    private String nombreUsu;
    private String contraseñaUsu;
    private String correoUsu;
    private Rango rango;
    private Rol rol;

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNombreUsu() {
        return nombreUsu;
    }

    public void setNombreUsu(String nombreUsu) {
        this.nombreUsu = nombreUsu;
    }

    public String getContraseñaUsu() {
        return contraseñaUsu;
    }

    public void setContraseñaUsu(String contraseñaUsu) {
        this.contraseñaUsu = contraseñaUsu;
    }

    public String getCorreoUsu() {
        return correoUsu;
    }

    public void setCorreoUsu(String correoUsu) {
        this.correoUsu = correoUsu;
    }

    public Rango getRango() {
        return rango;
    }

    public void setRango(Rango rango) {
        this.rango = rango;
    }

    public Rol getRol() {
        return rol;
    }

    public void setRol(Rol rol) {
        this.rol = rol;
    }

    

}
