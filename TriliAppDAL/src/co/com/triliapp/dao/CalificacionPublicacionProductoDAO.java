/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.triliapp.dao;

import co.com.triliapp.dto.CalificacionPublicacionProducto;
import co.com.triliapp.dto.PublicacionDeProducto;
import co.com.triliapp.dto.Usuario;
import co.com.triliapp.util.Conexion;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author Triliapp
 */
public class CalificacionPublicacionProductoDAO {

    public Conexion link;
    public PreparedStatement ps = null;
    CalificacionPublicacionProducto CalPD = new CalificacionPublicacionProducto();

    public CalificacionPublicacionProducto insertCalificacionPublicacionProducto(CalificacionPublicacionProducto CalPDI) throws SQLException {
        Conexion connect = new Conexion();
        String sql = "INSERT INTO CalificacionPublicacionProducto ( valorCPP ,  idUsuario , idPublicacionDeProducto) VALUES(?,?,?);";
       
            ps = connect.getConexion().prepareStatement(sql,PreparedStatement.RETURN_GENERATED_KEYS);
            ps.setFloat(1, CalPDI.getValorCPP());
            ps.setInt(2, CalPDI.getUsuario().getIdUsuario());
            ps.setInt(3, CalPDI.getPublicacionDeProducto().getIdPublicacionDeProducto());
            ps.executeUpdate();
            
            ResultSet rs = ps.getGeneratedKeys();
            rs.next();
            CalPDI.setIdCalificacionPublicacionProducto(rs.getInt(1));
                    
    return CalPDI;
    }

    public CalificacionPublicacionProducto EliminarCalificacionPublicacionProducto(CalificacionPublicacionProducto CalPDD) throws SQLException {
        Conexion connect = new Conexion();
        String sql = "DELETE FROM CalificacionPublicacionProducto WHERE idCalificacionPublicacionProducto=?;";      
            ps = connect.getConexion().prepareStatement(sql);
            ps.setInt(1, CalPDD.getIdCalificacionPublicacionProducto());
        return CalPDD;
    }

    public ArrayList<CalificacionPublicacionProducto> MostrarDatosCalificacionPublicacionProducto() throws SQLException {
        ArrayList<CalificacionPublicacionProducto> list = new ArrayList<>();
        Conexion connect = new Conexion();
        String sql = "SELECT * FROM calificacionpublicacionproducto;";
        ResultSet res = null;

            ps = connect.getConexion().prepareStatement(sql);
            res = ps.executeQuery();
            CalificacionPublicacionProducto CalPDAL = null;
            Usuario usu = new Usuario();
            PublicacionDeProducto pubP = new PublicacionDeProducto();
            UsuarioDAO usudao = new UsuarioDAO();
            PublicacionDeProductoDAO pubPDAO = new PublicacionDeProductoDAO();
            while (res.next()) {
                CalPDAL = new CalificacionPublicacionProducto();
                CalPDAL.setIdCalificacionPublicacionProducto(res.getInt(1));
                CalPDAL.setValorCPP(res.getFloat(2));
                CalPDAL.setFechaCPP(res.getTimestamp(3));
                usu = usudao.BuscarCodigoUsuario(res.getInt(4));
                CalPDAL.setUsuario(usu);
                pubP = pubPDAO.BuscarCodigoPublicacionDeProducto(res.getInt(5));
                CalPDAL.setPublicacionDeProducto(pubP);
                list.add(CalPDAL);
            }
        return list;
    }

    public CalificacionPublicacionProducto BuscarCodigoCalificacionPublicacionProducto(int CalPDB) throws SQLException {
        Conexion connect = new Conexion();
        String sql = "SELECT * FROM CalificacionPublicacionProducto WHERE idCalificacionPublicacionProducto=?;";
        ResultSet res = null;
        PreparedStatement ps = null;
        CalificacionPublicacionProducto CalPDBC = new CalificacionPublicacionProducto();    
            ps = connect.getConexion().prepareStatement(sql);
            ps.setInt(1, CalPDB);
            res = ps.executeQuery();
            res.next();
            CalPDBC.setIdCalificacionPublicacionProducto(res.getInt(1));
            CalPDBC.setValorCPP(res.getFloat(2));
            CalPDBC.setFechaCPP(res.getTimestamp(3));
            CalPDBC.getUsuario().setIdUsuario(res.getInt(4));
            CalPDBC.getPublicacionDeProducto().setIdPublicacionDeProducto(res.getInt(5));
        return CalPDBC;
    }
    public void modificarCalificacionPublicacionProducto(CalificacionPublicacionProducto CalPDU) throws SQLException {
        Conexion connect = new Conexion();
        String sql = "UPDATE calificacionpublicacionproducto SET  valorCPP=? , idUsuario=? , idPublicacionDeProducto=? WHERE idCalificacionPublicacionProducto = ?;";
        
            ps = connect.getConexion().prepareStatement(sql);
            ps.setFloat(1, CalPDU.getValorCPP());
            ps.setInt(2, CalPDU.getUsuario().getIdUsuario());
            ps.setInt(3, CalPDU.getPublicacionDeProducto().getIdPublicacionDeProducto());
            ps.setInt(4, CalPDU.getIdCalificacionPublicacionProducto());
            
    }
}
