package co.com.triliapp.dao;

import co.com.triliapp.dto.Producto;
import co.com.triliapp.dto.PublicacionDeProducto;
import co.com.triliapp.dto.Usuario;
import co.com.triliapp.util.Conexion;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.util.ArrayList;

public class PublicacionDeProductoDAO {

    public Conexion link;
    public PreparedStatement ps = null;
    PublicacionDeProducto PubDP = new PublicacionDeProducto();

    public PublicacionDeProducto insertarPublicacionDeProducto(PublicacionDeProducto PubDPI) throws SQLException {
        Conexion connect = new Conexion();
        String sql = "INSERT INTO publicaciondeproducto (nombrePDP  , descripcionPDP , imagenProducto , idUsuario, idProducto) VALUES(?,?,?,?,?);";
        
            ps = connect.getConexion().prepareStatement(sql,PreparedStatement.RETURN_GENERATED_KEYS );
            ps.setString(1, PubDPI.getNombrePDP());
            ps.setString(2, PubDPI.getDescripcionProducto());
            ps.setString(3, PubDPI.getImagenProducto());
            ps.setInt(4, PubDPI.getUsuario().getIdUsuario());
            ps.setInt(5, PubDPI.getProducto().getIdProducto());
            ps.executeUpdate();
            
            ResultSet rs = ps.getGeneratedKeys();
            rs.next();
            PubDPI.setIdPublicacionDeProducto(rs.getInt(1));
        

        return PubDPI;
    }

    //-------------------------------------Eliminar---------------------------//
    public PublicacionDeProducto EliminarPublicacionDeProductoID(PublicacionDeProducto PubDPE) throws SQLException {
        Conexion connect = new Conexion();
        String sql = "DELETE FROM PublicacionDeProducto WHERE idPublicacionDeProducto=?;";
        
            ps = connect.getConexion().prepareStatement(sql);
            ps.setInt(1, PubDPE.getIdPublicacionDeProducto());
            ps.executeUpdate();
        return PubDPE;
    }

    public PublicacionDeProducto EliminarPublicacionDeProductoNOM(PublicacionDeProducto PubDPE) throws SQLException {
        Conexion connect = new Conexion();
        String sql = "DELETE FROM PublicacionDeProducto WHERE nombrePDP=?;";  
            ps = connect.getConexion().prepareStatement(sql);
            ps.setString(1, PubDPE.getNombrePDP());
            ps.executeUpdate();
        return PubDPE;
    }

    public ArrayList<PublicacionDeProducto> MostrarDatosPublicacionDeProducto() throws SQLException {
        ArrayList<PublicacionDeProducto> list = new ArrayList<>();
        Conexion connect = new Conexion();
        String sql = "SELECT * FROM publicaciondeproducto;";
        ResultSet res = null;
        
            ps = connect.getConexion().prepareStatement(sql);
            res = ps.executeQuery();
            PublicacionDeProducto PubDPAL = null;
            Usuario usu = new Usuario();
            Producto prod = new Producto();
            UsuarioDAO usudao = new UsuarioDAO();
            ProductoDAO proddao= new ProductoDAO();
            while (res.next()) {
               
                PubDPAL = new PublicacionDeProducto();
                PubDPAL.setIdPublicacionDeProducto(res.getInt(1));
                PubDPAL.setNombrePDP(res.getString(2));
                PubDPAL.setFechaPDP(res.getTimestamp(3));
                PubDPAL.setDescripcionProducto(res.getString(4));
                PubDPAL.setImagenProducto(res.getString(5));
                usu = usudao.BuscarCodigoUsuario(res.getInt(6));
                PubDPAL.setUsuario(usu);
                prod = proddao.buscarPorCodigoProducto(res.getInt(7));
                PubDPAL.setProducto(prod);
                
                list.add(PubDPAL);

            }
        return list;
    }

    public PublicacionDeProducto BuscarCodigoPublicacionDeProducto(int PubDPB) throws SQLException {
        Conexion connect = new Conexion();
        String sql = "SELECT * FROM PublicacionDeProducto WHERE idPublicacionDeProducto=?;";
        ResultSet res = null;
        PreparedStatement ps = null;
        PublicacionDeProducto PubBDBC = null;
       
            PubBDBC = new PublicacionDeProducto();
            ps = connect.getConexion().prepareStatement(sql);
            ps.setInt(1, PubDPB);
            res = ps.executeQuery();
            
            res.next();
            PubBDBC.setIdPublicacionDeProducto(res.getInt(1));
            PubBDBC.setNombrePDP(res.getString(2));
            PubBDBC.setFechaPDP(res.getTimestamp(3));
            PubBDBC.setDescripcionProducto(res.getString(2));
            PubBDBC.setImagenProducto(res.getString(2));
            PubBDBC.getUsuario().setIdUsuario(res.getInt(4));
            PubBDBC.getProducto().setIdProducto(res.getInt(5));
            
        
        return PubBDBC;
    }

    //------------------------------Modificar---------------------------------//
    public void modificarPublicacionDeProducto(PublicacionDeProducto PibDPU) throws SQLException {
        Conexion connect = new Conexion();
        String sql = "UPDATE PublicacionDeProducto SET  nombrePDP =? , descripcionPDP=? , imagenProducto=? , idUsuario= ? , idProducto= ? WHERE idPublicacionDeProducto = ?;";
        
            ps = connect.getConexion().prepareStatement(sql);   
            ps.setString(1, PibDPU.getNombrePDP());
            ps.setString(2, PibDPU.getDescripcionProducto());
            ps.setString(3, PibDPU.getImagenProducto());
            ps.setInt(4, PibDPU.getUsuario().getIdUsuario());
            ps.setInt(5, PibDPU.getProducto().getIdProducto());
            ps.setInt(6, PibDPU.getIdPublicacionDeProducto());

            ps.executeUpdate();
    }

}
