package co.com.triliapp.dao;

import co.com.triliapp.dto.CalificacionDeProducto;
import co.com.triliapp.dto.Producto;
import co.com.triliapp.dto.Usuario;
import co.com.triliapp.util.Conexion;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author Triliapp
 */
public class CalificacionDeProductoDAO {

    public Conexion link;
    public PreparedStatement ps = null;
    CalificacionDeProducto CalProd = new CalificacionDeProducto();

    //-----------------------------------Insert a cada uno de los atributos de CalificacionDeProducto ----------------------------------------------------------------------------------------//
    public CalificacionDeProducto insertCalificacionDeProducto(CalificacionDeProducto CalProdI) throws SQLException {
        Conexion connect = new Conexion();
        String sql = "INSERT INTO calificaciondeproducto ( valorCPR , idUsuario , idProducto) VALUES(?,?,?);";
    
            ps = connect.getConexion().prepareStatement(sql,PreparedStatement.RETURN_GENERATED_KEYS);
            ps.setDouble(1, CalProdI.getValorCPR());
            ps.setInt(2, CalProdI.getUsuario().getIdUsuario());
            ps.setInt(3, CalProdI.getProducto().getIdProducto());
            ps.executeUpdate();
            
            ResultSet rs = ps.getGeneratedKeys();
            rs.next();
            CalProdI.setIdCalificacionDeProducto(rs.getInt(1));
           
        return CalProdI;
    }

    //----------------------------------------Elimminar-----------------------//
    public CalificacionDeProducto EliminarCodigoCalificacionDeProducto(CalificacionDeProducto CalProdD) throws SQLException {
        Conexion connect = new Conexion();
        String sql = "DELETE FROM CalificacionDeProducto WHERE idCalificacionDeProducto=?;";
            ps = connect.getConexion().prepareStatement(sql);
            ps.setInt(1, CalProdD.getIdCalificacionDeProducto());
            ps.executeUpdate();           
        return CalProdD;
    }

    public ArrayList<CalificacionDeProducto> MostrarDatosCalificacionDeProducto() throws SQLException {
        ArrayList<CalificacionDeProducto> list = new ArrayList<>();
        Conexion connect = new Conexion();
        String sql = "SELECT * FROM calificaciondeproducto;";
        ResultSet res = null;
        PreparedStatement ps = null;
                  
            ps = connect.getConexion().prepareStatement(sql);
            res = ps.executeQuery();
            
            CalificacionDeProducto CalProdAL = null;
            
            Usuario usu = new Usuario();
            Producto prod = new Producto();
            UsuarioDAO usudao = new UsuarioDAO();
            ProductoDAO proddao = new ProductoDAO();
            while (res.next()) {
                CalProdAL = new CalificacionDeProducto();
                
                CalProdAL.setIdCalificacionDeProducto(res.getInt(1));
                CalProdAL.setValorCPR(res.getFloat(2));
                CalProdAL.setFechaCPR(res.getTimestamp(3));
                usu = usudao.BuscarCodigoUsuario(res.getInt(4));
                CalProdAL.setUsuario(usu);
                prod = proddao.buscarPorCodigoProducto(res.getInt(5));
                CalProdAL.setProducto(prod);
                
                list.add(CalProdAL);
            }      
        return list;
    }

    public CalificacionDeProducto BuscarCodigoCalificacionDeProducto(int CalProdB) throws SQLException {
        Conexion connect = new Conexion();
        String sql = "SELECT * FROM calificacionproducto WHERE idCalificacionDeProducto=?;";
        ResultSet res = null;
        PreparedStatement ps = null;
        CalificacionDeProducto CalProdBC = new CalificacionDeProducto();
        
            ps = connect.getConexion().prepareStatement(sql);
            ps.setInt(1, CalProdB);
            res = ps.executeQuery();
            
            res.next();
            CalProdBC.setIdCalificacionDeProducto(res.getInt(1));
            CalProdBC.setValorCPR(res.getFloat(2));
            CalProdBC.setFechaCPR(res.getTimestamp(3));
            CalProdBC.getUsuario().setIdUsuario(res.getInt(4));
            CalProdBC.getProducto().setIdProducto(res.getInt(5));

        return CalProdBC;
    }

    //-------------------------Actualizar PublicacionProducto-----------------//
    public void modificarCalificacionDeProducto(CalificacionDeProducto CalProdU) throws SQLException {
        Conexion connect = new Conexion();
        String sql = "UPDATE calificacionproducto SET   valorCPR=? ,  idUsuario=? , idProducto=? WHERE idCalificacionProducto=?;";
        
            ps = connect.getConexion().prepareStatement(sql);
            ps.setDouble(1, CalProdU.getValorCPR());
            ps.setInt(2, CalProdU.getUsuario().getIdUsuario());
            ps.setInt(3, CalProdU.getProducto().getIdProducto());
            ps.setInt(4, CalProdU.getIdCalificacionDeProducto());
            ps.executeUpdate();                     
    }
}
