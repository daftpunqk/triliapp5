package co.com.triliapp.dao;

import co.com.triliapp.dto.Rango;
import co.com.triliapp.dto.Rol;
import co.com.triliapp.dto.Usuario;//-------------------Paquete Categoria
import co.com.triliapp.util.Conexion;//-----------------Paquete Conexion
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class UsuarioDAO {

    Usuario usuario = new Usuario();
    public Conexion link;
    public PreparedStatement ps = null;

    //----------------------------Insertar Usuario----------------------------//
    public Usuario insertarUsuario(Usuario usu) throws SQLException {
        Conexion connect = new Conexion();
        String sql = "INSERT INTO Usuario (nombreUsu,contraseñaUsu ,correoUsu, idRango, idRol ) VALUES(?,?,?,?,?);";
        ps = connect.getConexion().prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);//=======generar la pk

        ps.setString(1, usu.getNombreUsu());
        ps.setString(2, usu.getContraseñaUsu());
        ps.setString(3, usu.getCorreoUsu());
        ps.setInt(4, usu.getRango().getIdRango());
        ps.setInt(5, usu.getRol().getIdRol());
        
        ps.executeUpdate();
        //===============================Retornar pk==========================//
        ResultSet rs = ps.getGeneratedKeys();
        rs.next();
        usu.setIdUsuario(rs.getInt(1));
        //=====================================================================      

        return usu;
    }

    //----------------------------------Mostrar Datos Usuario---------------//
    public ArrayList<Usuario> MostrarUsuario() throws SQLException {
        ArrayList<Usuario> list = new ArrayList<>();
        Conexion connect = new Conexion();

        String sql = "SELECT * FROM Usuario;";
        ResultSet res = null;
        

        ps = connect.getConexion().prepareStatement(sql);
        res = ps.executeQuery();
        RangoDAO rdao = new RangoDAO();
        RolDAO rolDAO = new RolDAO();
        Usuario usu = null;
        Rango rang = null;
        Rol rol = null;
        while (res.next()) {

            usu = new Usuario();
            usu.setIdUsuario(res.getInt(1));
            usu.setNombreUsu(res.getString(2));
            usu.setContraseñaUsu(res.getString(3));
            usu.setCorreoUsu(res.getString(4));
            rang = rdao.BuscarCodigoRango(res.getInt(5));
            usu.setRango(rang);
            rol = rolDAO.BuscarCodigoRol(res.getInt(6));
            usu.setRol(rol);

            list.add(usu);
        }   
        return list;
    }

    //-------------------------------Buscar Codigo Usuario-------------------//
    public Usuario BuscarCodigoUsuario(int idUsuario) throws SQLException {
        Conexion connect = new Conexion();
        String sql = "SELECT * FROM Usuario WHERE idUsuario=?;";
        ResultSet rs = null;

        Usuario usuarioVar = new Usuario();
        
        RangoDAO rangDAO = new RangoDAO();
        RolDAO rolDAO = new RolDAO();
        
        Rango rang = null;
        Rol rol = null;
        

        ps = connect.getConexion().prepareStatement(sql);
        
        ps.setInt(1, idUsuario);
        rs = ps.executeQuery();
        rs.next();
        
        usuarioVar.setIdUsuario(rs.getInt(1));
        usuarioVar.setNombreUsu(rs.getString(2));
        usuarioVar.setContraseñaUsu(rs.getString(3));
        usuarioVar.setCorreoUsu(rs.getString(4));
        rang = rangDAO.BuscarCodigoRango(rs.getInt(5));
        usuarioVar.setRango(rang);
        rol =  rolDAO.BuscarCodigoRol(rs.getInt(6));
        usuarioVar.setRol(rol);

        return usuarioVar;
    }

    //-------------------------------ELIMINAR Codigo Usuario-------------------//
    public Usuario eliminarUsuario(Usuario usu) throws SQLException {

        Conexion connect = new Conexion();
        String sql = "DELETE FROM Usuario WHERE idUsuario = ?;";
        PreparedStatement ps = null;

        ps = connect.getConexion().prepareStatement(sql);
        ps.setInt(1, usu.getIdUsuario());
        ps.executeUpdate();

        return usu;
    }

    //--------------------------------Actualizar Nombre Usuario----------------------//
    public void modificarNombreUsuario(Usuario Usu) throws SQLException {
        Conexion connect = new Conexion();
        String sql = "UPDATE Usuario SET   nombreUsu = ? , contraseñaUsu = ? ,  correoUsu = ? WHERE idUsuario = ?;";

        ps = connect.getConexion().prepareStatement(sql);
        ps.setString(1, Usu.getNombreUsu());
        ps.setString(2, sql);
        ps.setInt(2, Usu.getIdUsuario());
        ps.executeUpdate();
    }

    //-------------------------------------Actualizar Usuario-----------------//
    public void modificarUsuario(Usuario Usu) throws SQLException {
        Conexion connect = new Conexion();
        String sql = "UPDATE Usuario SET   nombreUsu = ? ,contraseñaUsu = ? , correoUsu = ?WHERE idUsuario = ?;";

        ps = connect.getConexion().prepareStatement(sql);
        ps.setString(1, Usu.getNombreUsu());
        ps.setString(2, Usu.getContraseñaUsu());
        ps.setString(3, Usu.getCorreoUsu());
        ps.setInt(4, Usu.getIdUsuario());

        
        ps.executeUpdate();

    }

    public void BuscarNombreUsuarioAcceder(Usuario nombreU, Usuario contraU) {
        Conexion connect = new Conexion();
        String sql = "SELECT * FROM Usuario WHERE nombreUsu=? && contraseñaUsu = ?;";
        Integer Cap = 0;
        try {

            ps = connect.getConexion().prepareStatement(sql);
            ps.setString(1, nombreU.getNombreUsu());
            ps.setString(2, contraU.getContraseñaUsu());
            ResultSet rs = ps.executeQuery();

            rs.next();
            Cap = rs.getInt("idRol");

            if (Cap.equals(2)) {
                System.out.println("Ha ingresado como Usuario Administrador");
            }
            if (Cap.equals(1)) {
                System.out.println("Ha ingresado como Usuario Invitado");
            }
            if ((!Cap.equals(2)) && (!Cap.equals(1))) {
                System.out.println("Usuario y/o contraseña no validos");
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());

        }

    }
}
