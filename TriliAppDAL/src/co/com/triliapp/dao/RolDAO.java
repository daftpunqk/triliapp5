/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.triliapp.dao;

import co.com.triliapp.dto.Rol;
import co.com.triliapp.util.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author yasuly
 */
public class RolDAO {
    
    private Rol Rol;
    public Conexion link;
    public PreparedStatement ps = null;
    
    //-----------------------------Insertar Rol
    public Rol insertarRol (Rol rolI) throws SQLException{
      Conexion connect = new Conexion() ;
        String sql = "INSERT INTO Rol ( nombreRL ) VALUES(?);";
        ps = connect.getConexion().prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
    
            ps.setString(1, rolI.getNombreRL());
            
            ps.executeUpdate();
            
            ResultSet rs = ps.getGeneratedKeys();
            rs.next();
            rolI.setIdRol(rs.getInt(1));
            
    return rolI;
    }
    
    //--------------------Mostar Datos Rol
    
    public ArrayList<Rol> MostrarRol() throws SQLException {
        ArrayList<Rol> list = new ArrayList<>();
        Conexion connect = new Conexion();

        String sql = "SELECT * FROM Rol;";
        ResultSet rs = null;
       
            ps = connect.getConexion().prepareStatement(sql);
            rs = ps.executeQuery();
            Rol RoD = null;
            
            while (rs.next()) {
                
                RoD = new Rol();
               
                RoD.setIdRol(rs.getInt(1));
                RoD.setNombreRL(rs.getString(2));
                
                list.add(RoD);
            }
        
        return list;
    }
    
    //----------------Buscar Codigo Rol
    
    public Rol BuscarCodigoRol(int rolB) throws SQLException {
        Conexion connect = new Conexion();
        String sql = "SELECT * FROM Rol WHERE idRol=?;";
        ResultSet rs = null;
        PreparedStatement ps = null;
        Rol rol = new Rol();
        
        rol = new Rol ();
            ps = connect.getConexion().prepareStatement(sql);
            ps.setInt(1, rolB);
            rs = ps.executeQuery();
            
            rs.next();
            rol.setIdRol(rs.getInt(1));
            rol.setNombreRL(rs.getString(2));
            
            return rol;
    }
    
    //----------Eliminar Codigo Rol
    
    public Rol eliminarRol(Rol rolD) throws SQLException{

        Conexion connect = new Conexion() ;
        String sql = "DELETE FROM Rol WHERE idRol = ?;";
        
            ps = connect.getConexion().prepareStatement(sql);
            ps.setInt(1, rolD.getIdRol());
            ps.executeUpdate();

        return rolD;
    }
    
    //--------------------Modificar
    
        public void modificarRoL (Rol rolM) throws SQLException{
        Conexion connect = new Conexion();
        String sql = "UPDATE Rol SET   nombreRL = ? WHERE idRol = ?;";

            ps = connect.getConexion().prepareStatement(sql);
            ps.setString(1, rolM.getNombreRL());
            ps.setInt(2, rolM.getIdRol());
            
            ps.executeUpdate();

    }
}
