package triliapptest;

import co.com.triliapp.dto.Usuario;
import co.com.triliapp.dto.Producto;
//----------------------------------------------------------------------------//
import co.com.triliapp.dto.CalificacionDeProducto;
import co.com.triliapp.dao.CalificacionDeProductoDAO;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TriliAppTestCalificacionDeProducto {

    public static void main(String[] args) {

        TriliAppTestCalificacionDeProducto triliAppTesCP = new TriliAppTestCalificacionDeProducto();

        triliAppTesCP.insertarCalificacionDeProducto();
//        triliAppTesCP.eliminarPublicacionDeProducto();
//        triliAppTesCP.modificarCalificacionDeProducto();
        triliAppTesCP.mostrarCalificacionDeProducto();

    }

    //-------------------------------Insertar---------------------------------//
    public void insertarCalificacionDeProducto() {

        Usuario Usu = new Usuario();
        Usu.setIdUsuario(2);

        Producto Prod = new Producto();
        Prod.setIdProducto(1);

        CalificacionDeProducto Cdp = new CalificacionDeProducto();
        Cdp.setValorCPR(5);
        Cdp.setUsuario(Usu);
        Cdp.setProducto(Prod);

        CalificacionDeProductoDAO cpdDAO = new CalificacionDeProductoDAO();
        
        try {
            cpdDAO.insertCalificacionDeProducto(Cdp);
            System.out.println("CALIFICACION DE PRODUCTO HECHA"+ Cdp.getIdCalificacionDeProducto());
        } catch (SQLException ex) {
            Logger.getLogger(TriliAppTestCalificacionDeProducto.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //-----------------------------------Eliminar-----------------------------//
    public void eliminarPublicacionDeProducto() {

        CalificacionDeProducto Cdp = new CalificacionDeProducto();
        Cdp.setIdCalificacionDeProducto(1);

    
            CalificacionDeProductoDAO cdpDAO = new CalificacionDeProductoDAO();
            
        try {
            cdpDAO.EliminarCodigoCalificacionDeProducto(Cdp);
            System.out.println("CALIFICACION DE PRODUCTO ELIMINADA");
        } catch (SQLException ex) {
            Logger.getLogger(TriliAppTestCalificacionDeProducto.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    //--------------------------------------Modificar-------------------------//
    public void modificarCalificacionDeProducto() {

        Usuario Usu = new Usuario();
        Usu.setIdUsuario(4);

        Producto Prod = new Producto();
        Prod.setIdProducto(1);

        CalificacionDeProducto Cdp = new CalificacionDeProducto();

        Cdp.setIdCalificacionDeProducto(3);
        Cdp.setValorCPR(890000);
        Cdp.setUsuario(Usu);
        Cdp.setProducto(Prod);

        CalificacionDeProductoDAO cdpDAO = new CalificacionDeProductoDAO();
        
        try {
            cdpDAO.modificarCalificacionDeProducto(Cdp);
            System.out.println("CALIFICACION DE PRODUCTO MODIFICADA");
        } catch (SQLException ex) {
            Logger.getLogger(TriliAppTestCalificacionDeProducto.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //----------------------------Mostrar-------------------------------------//
    public void mostrarCalificacionDeProducto() {

       
            CalificacionDeProductoDAO cdpDAO = new CalificacionDeProductoDAO();
        try {
            for (CalificacionDeProducto e : cdpDAO.MostrarDatosCalificacionDeProducto()) {
                System.out.println("DATOS CALIFICACION DE PRODUCTO");
                System.out.println(e.getIdCalificacionDeProducto() + " " + e.getValorCPR() + " " + e.getFechaCPR());
            }
        } catch (SQLException ex) {
            Logger.getLogger(TriliAppTestCalificacionDeProducto.class.getName()).log(Level.SEVERE, null, ex);
        }
            
        
    }
}
