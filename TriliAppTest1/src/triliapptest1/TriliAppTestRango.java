package triliapptest;

import co.com.triliapp.dto.Rango;
import co.com.triliapp.dao.RangoDAO;
//----------------------------------------------------------------------------//
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;



public class TriliAppTestRango {
    
    public static void main(String[] args) {
        
        TriliAppTestRango triliAppRang = new  TriliAppTestRango();
        
        triliAppRang.insertarRango();
//        triliAppRang.mostrarRango();
//        triliAppRang.eliminarRango();
//        triliAppRang.modificarRango();
        
    }
   //---------------------------Insertar-------------------------------------//
    public void insertarRango() {

        Rango rang = new Rango();       

        rang.setNombreRango("Oro");
        rang.setImagenRNG("not image");
        rang.setDescripcionRNG("not ");

        RangoDAO ranDao = new RangoDAO();
       try {
            ranDao.insertarRango(rang);
            System.out.println("Rango Insertada"+ rang.getIdRango());
        } catch (SQLException ex) {
            Logger.getLogger(TriliAppTestRango.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //------------------------------Mostrar-----------------------------------//
    public void mostrarRango() {

    RangoDAO ranDao = new RangoDAO();

        try {   
            for(Rango e : ranDao.MostrarRango()){
                System.out.println("Rango Publicada");
                System.out.println(e.getIdRango()+ " " + e.getNombreRNG()+" "+ e.getImagenRNG()+" "+e.getDescripcionRNG());
            }
        }catch (SQLException ex) {
            Logger.getLogger(TriliAppTestRango.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //--------------------------Eliminar--------------------------------------//
    public void eliminarRango() {
 
    Rango Rang = new Rango();
    Rang.setIdRango(8);
    RangoDAO RangD = new RangoDAO();
    Rango dt;
    
        try {
        dt = RangD.eliminarRango(Rang);
           System.out.print("Rango Eliminada");
        } catch (SQLException ex) {
            Logger.getLogger(TriliAppTestRango.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
//--------------Actualizar-----------------------------------//

    public void modificarRango() {
        Rango RNG = new Rango();
        
        
        RNG.setIdRango(8);
        RNG.setNombreRango("diamante");
        RNG.setImagenRNG("buena imagen");
        RNG.setDescripcionRNG("eres bueno ");

        RangoDAO ranDao = new RangoDAO();
        try{
            ranDao.modificarRango(RNG);
            System.out.println("Rango Modificada");  
        }catch (SQLException ex) {
            Logger.getLogger(TriliAppTestRango.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
