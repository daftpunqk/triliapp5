package triliapptest;

import co.com.triliapp.dto.Rango;
import co.com.triliapp.dto.Usuario;
import co.com.triliapp.dao.UsuarioDAO;
import co.com.triliapp.dto.Rol;
import java.sql.SQLException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TriliAppTestUsuario {

    public static void main(String[] args) {

        TriliAppTestUsuario triliAppUsu = new TriliAppTestUsuario();

//          triliAppUsu.Acceder();
          triliAppUsu.insertarUsuario();
          triliAppUsu.mostrarUsuario();
//          triliAppUsu.eliminarUsuario();
//          triliAppUsu.ActualizarNombreUsuario();
//          triliAppUsu.actualizarUsuario();
//          triliAppUsu.mostrarProductoPorCodigo(); 


         

    }

    //-----------------------------Insertar-----------------------------------//
    public void insertarUsuario() {
        Usuario usu = new Usuario();
        Rango rang = new Rango();
        Rol rolU = new Rol();
        rang.setIdRango(1);
        rolU.setIdRol(2);

        usu.setNombreUsu("prueba5");
        usu.setContraseñaUsu("/break/");
        usu.setCorreoUsu("Carrera norte # 19");
        usu.setRango(rang);
        usu.setRol(rolU);
        UsuarioDAO usuDao = new UsuarioDAO();
        try {
            usuDao.insertarUsuario(usu);
            System.out.println(usu.getIdUsuario());
        } catch (SQLException ex) {
            Logger.getLogger(TriliAppTestUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //--------------------------Mostrar---------------------------------------//
    public void mostrarUsuario() {
        UsuarioDAO usuD = new UsuarioDAO();
        Rango rang = null;
        try {
            rang = new Rango();
            for (Usuario e : usuD.MostrarUsuario()) {
                System.out.println(e.getIdUsuario() + " " + e.getNombreUsu() + " " + e.getContraseñaUsu() + " " + e.getCorreoUsu() + " " + e.getRango().getIdRango() + " " + e.getRol().getIdRol());
            }
        } catch (SQLException ex) {
            Logger.getLogger(TriliAppTestUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //==============================MostraPorCodigo===========================//
    public void mostrarProductoPorCodigo() {
        UsuarioDAO usuDAO = new UsuarioDAO();
        Usuario Usu;
        try {
            Usu = usuDAO.BuscarCodigoUsuario(3);
            System.out.println(Usu.getIdUsuario() + " " + Usu.getNombreUsu() + " " + Usu.getContraseñaUsu() + " " + Usu.getCorreoUsu() + " " + Usu.getRango().getIdRango() + " " + Usu.getRol().getIdRol());
        } catch (SQLException ex) {
            Logger.getLogger(TriliAppTestProducto.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //----------------------Eliminar------------------------------------------//
    public void eliminarUsuario() {

        Usuario usu = new Usuario();
        usu.setIdUsuario(5);

        UsuarioDAO usuD = new UsuarioDAO();
        Usuario dto;

        try {
            dto = usuD.eliminarUsuario(usu);
            System.out.println("USUARIO ELIMIADO");
        } catch (SQLException ex) {
            Logger.getLogger(TriliAppTestUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    //----------------------------Modificar Nombre Usuario--------------------//
    public void ActualizarNombreUsuario() {
        Usuario usu = new Usuario();

        usu.setIdUsuario(1);
        usu.setNombreUsu("pepito");
        UsuarioDAO usuDao = new UsuarioDAO();
        try {
            usuDao.modificarNombreUsuario(usu);
            System.out.println("DATOS ACTUALIZADOS");
        } catch (SQLException ex) {
            Logger.getLogger(TriliAppTestUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    //----------------------------Modificar Usuario---------------------------//
    public void actualizarUsuario() {
        Usuario usu = new Usuario();

        usu.setIdUsuario(2);
        usu.setNombreUsu("Daftpunqk");
        usu.setContraseñaUsu("PEPITO11");
        usu.setCorreoUsu("calle 41 bis sur");

        UsuarioDAO usuDao = new UsuarioDAO();

        try {
            usuDao.modificarUsuario(usu);
            System.out.println("USUARIO ACTUALIZADO");
        } catch (SQLException ex) {
            Logger.getLogger(TriliAppTestUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //==================================Acceder===============================//
    public void Acceder() {

        Usuario usu = new Usuario();
        usu.setNombreUsu("usux");
        usu.setContraseñaUsu("usux");
        UsuarioDAO usuDaoA = new UsuarioDAO();
        usuDaoA.BuscarNombreUsuarioAcceder(usu, usu);

    }

}
