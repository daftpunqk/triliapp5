package triliapptest;

import co.com.triliapp.dto.Usuario;
//============================================================================//
import co.com.triliapp.dao.SugerenciaDAO;
import co.com.triliapp.dto.Sugerencia;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TriliAppTestSugerencia {

    public static void main(String[] args) {

        TriliAppTestSugerencia triliAppTestSUG = new TriliAppTestSugerencia();

        //====================================================================//
        triliAppTestSUG.insertarSugerencia();
//        triliAppTestSUG.eliminarSugerencia();
//        triliAppTestSUG.modificarSugerencia();
//        triliAppTestSUG.mostrarSugerencia();

    }

//    -----------------------------Insertar-----------------------------------//
    public void insertarSugerencia() {

        Sugerencia Sug = new Sugerencia();

        Usuario Usu = new Usuario();
        Usu.setIdUsuario(2);

        Sug.setTipoSug("text y imagen");
        Sug.setDescripcionSug("descripcion sugerencia");
        Sug.setUsuario(Usu);
    
        SugerenciaDAO SugDao = new SugerenciaDAO();
        
        try {
            SugDao.insertarSugerencia(Sug);
            System.out.println("Sugerencia Insertada"+ Sug.getIdSugerencia());
        } catch (SQLException ex) {
            Logger.getLogger(TriliAppTestSugerencia.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
//
//    //-------------------------------Eliminar---------------------------------//
    public void eliminarSugerencia() {

        Sugerencia Sug = new Sugerencia();
        Sug.setIdSugerencia(7);
        SugerenciaDAO SugDAO = new SugerenciaDAO();
        Sugerencia dt;

        try {
           dt = SugDAO.EliminarSugerencia(Sug);
           System.out.print("Sugerencia Eliminada");
        } catch (SQLException ex) {
               Logger.getLogger(TriliAppTestSugerencia.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
//
//    //----------------------------Modificar-----------------------------------//
    public void modificarSugerencia() {

            Sugerencia Sug = new Sugerencia();

        Usuario Usu = new Usuario();
        Usu.setIdUsuario(4);

        Sug.setIdSugerencia(7);
        Sug.setTipoSug("imagen");
        Sug.setDescripcionSug("descrip actu");
        Sug.setUsuario(Usu);
        
        SugerenciaDAO SugDAO = new SugerenciaDAO();
        try{
            SugDAO.modificarSugerencia(Sug);
            System.out.println("Sugerencia Modificada");  
        } catch (SQLException ex) {
               Logger.getLogger(TriliAppTestSugerencia.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

//    //-----------------------------Mostrar------------------------------------//
    public void mostrarSugerencia() {
        
        SugerenciaDAO SugDAO = new SugerenciaDAO();
        
        try {
            for(Sugerencia e : SugDAO.MostrarDatoSugerencia()){
                System.out.println("Sugerencia Publicada");
                System.out.println(e.getIdSugerencia()+" "+e.getTipoSug()+" "+e.getFechaSug()+" "+e.getDescripcionSug()+" "+e.getUsuario().getIdUsuario());
            }
        } catch (SQLException ex) {
        Logger.getLogger(TriliAppTestSugerencia.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
