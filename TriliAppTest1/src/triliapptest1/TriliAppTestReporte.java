package triliapptest;

import co.com.triliapp.dto.Usuario;
import co.com.triliapp.dto.PublicacionDeProducto;
//============================================================================//
import co.com.triliapp.dao.ReporteDAO;
import co.com.triliapp.dto.Reporte;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TriliAppTestReporte {

    public static void main(String[] args) {

        TriliAppTestReporte triliAppTestRPT = new TriliAppTestReporte();

        //====================================================================//
        triliAppTestRPT.insertarReporte();
//        triliAppTestRPT.eliminarReporte();
//        triliAppTestRPT.modificarReporte();
//        triliAppTestRPT.mostrarReporte();

    }

    //-----------------------------Insertar-----------------------------------//
    public void insertarReporte() {

        Reporte Rpte = new Reporte();

        Usuario Usu = new Usuario();
        Usu.setIdUsuario(2);
        PublicacionDeProducto PDP = new PublicacionDeProducto();
        PDP.setIdPublicacionDeProducto(1);

        Rpte.setDescripcionRpte("descripcion no sirve la informacion");
        Rpte.setUsuario(Usu);
        Rpte.setPublicacionDeProducto(PDP);
    

        ReporteDAO RpteDao = new ReporteDAO();
         try {
            RpteDao.insertReporte(Rpte);
            System.out.println("Sugerencia Insertada"+ Rpte.getIdReporte());
        } catch (SQLException ex) {
            Logger.getLogger(TriliAppTestReporte.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //-------------------------------Eliminar---------------------------------//
    public void eliminarReporte() {

        Reporte Rpt = new Reporte();
        Rpt.setIdReporte(9);
        ReporteDAO RpteDAO = new ReporteDAO();
        Reporte dt;

        try {
          dt = RpteDAO.EliminarReporte(Rpt);
            System.out.println("Reporte Eliminado");
        } catch (SQLException ex) {
            Logger.getLogger(TriliAppTestReporte.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //----------------------------Modificar-----------------------------------//
    public void modificarReporte() {

            Reporte Rpte = new Reporte();

        Usuario Usu = new Usuario();
        Usu.setIdUsuario(4);
        PublicacionDeProducto PDP = new PublicacionDeProducto();
        PDP.setIdPublicacionDeProducto(5);

        Rpte.setIdReporte(9);
        Rpte.setDescripcionRpte("Pueba es poco informativo");
        Rpte.setUsuario(Usu);
        Rpte.setPublicacionDeProducto(PDP);
   
        ReporteDAO RpteDAO = new ReporteDAO();
        try{
            RpteDAO.modificarReporte(Rpte);
            System.out.println("Reporte Modificado");  
        }catch (SQLException ex) {
            Logger.getLogger(TriliAppTestReporte.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //-----------------------------Mostrar------------------------------------//
    public void mostrarReporte() {
        
    ReporteDAO RpteDAO = new ReporteDAO();   
    
        try {

            for(Reporte e : RpteDAO.MostrarDatoReporte()){
                System.out.println(e.getIdReporte()+" "+e.getDescripcionRpte()+" "+e.getFechaRpte()+" "+e.getUsuario().getIdUsuario()+" "+e.getPublicacionDeProducto().getIdPublicacionDeProducto());
            }
        } catch (SQLException ex) {
            Logger.getLogger(TriliAppTestReporte.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
