package triliapptest;

import co.com.triliapp.dto.Usuario;
import co.com.triliapp.dto.Producto;
//============================================================================//
import co.com.triliapp.dto.PublicacionDeProducto;
import co.com.triliapp.dao.PublicacionDeProductoDAO;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TriliAppTestPublicacionDeProducto {

    public static void main(String[] args) {

        TriliAppTestPublicacionDeProducto triliAppTestPDP = new TriliAppTestPublicacionDeProducto();

        //====================================================================//
        triliAppTestPDP.insertarPublicacionDeProducto();
//        triliAppTestPDP.eliminarPublicacionDeProductoID(); // por ID
//        triliAppTestPDP.modificarPublicacionDeProducto();
//        triliAppTestPDP.mostrarPublicacionDeProducto();
//        triliAppTestPDP.eliminarPublicacionDeProductoNOM(); // por NOMBRE

    }

    //-----------------------------Insertar-----------------------------------//
    public void insertarPublicacionDeProducto() {

        PublicacionDeProducto Pdp = new PublicacionDeProducto();

        Usuario Usu = new Usuario();
        Usu.setIdUsuario(2);

        Producto Prod = new Producto();
        Prod.setIdProducto(1);

        Pdp.setNombrePDP("Prueba 1");
        Pdp.setDescripcionProducto("descripcion Publicacion");
        Pdp.setImagenProducto("No hay Imagen");
        Pdp.setUsuario(Usu);
        Pdp.setProducto(Prod);

        PublicacionDeProductoDAO pdpDao = new PublicacionDeProductoDAO();
        
        try {
            pdpDao.insertarPublicacionDeProducto(Pdp);
            System.out.println("PUBLICACION INSERTADA"+ Pdp.getIdPublicacionDeProducto());
        } catch (SQLException ex) {
            Logger.getLogger(TriliAppTestPublicacionDeProducto.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //-------------------------------Eliminar---------------------------------//
    public void eliminarPublicacionDeProductoID() {

        PublicacionDeProducto Pdp = new PublicacionDeProducto();
        Pdp.setIdPublicacionDeProducto(2);
      
        PublicacionDeProductoDAO pdpDAO = new PublicacionDeProductoDAO();
            
        try {
            pdpDAO.EliminarPublicacionDeProductoID(Pdp);
            System.out.println("PUBLICACION ELIMINADA");
        } catch (SQLException ex) {
            Logger.getLogger(TriliAppTestPublicacionDeProducto.class.getName()).log(Level.SEVERE, null, ex);
        }
      
    }
    public void eliminarPublicacionDeProductoNOM() {

        PublicacionDeProducto Pdp = new PublicacionDeProducto();
        Pdp.setNombrePDP("nombre publicacion borrar");     
        PublicacionDeProductoDAO pdpDAO = new PublicacionDeProductoDAO();        
        try {
            pdpDAO.EliminarPublicacionDeProductoNOM(Pdp);
            System.out.println("PUBLICACION ELIMINADA");
        } catch (SQLException ex) {
            Logger.getLogger(TriliAppTestPublicacionDeProducto.class.getName()).log(Level.SEVERE, null, ex);
        }
      
    }

    //----------------------------Modificar-----------------------------------//
    public void modificarPublicacionDeProducto() {

        PublicacionDeProducto Pdp = new PublicacionDeProducto();

        Usuario Usu = new Usuario();
        Usu.setIdUsuario(4);
        Producto prod = new Producto();
        prod.setIdProducto(1);

        Pdp.setIdPublicacionDeProducto(5);
        Pdp.setNombrePDP("Pueba Update");
        Pdp.setDescripcionProducto("Descripcion Actualizada");
        Pdp.setImagenProducto("Imagen Update");
        Pdp.setUsuario(Usu);
        Pdp.setProducto(prod);

        PublicacionDeProductoDAO pdpDAO = new PublicacionDeProductoDAO();
        try {
            pdpDAO.modificarPublicacionDeProducto(Pdp);
            System.out.println("PUBLICACION MODIFICADA");
        } catch (SQLException ex) {
            Logger.getLogger(TriliAppTestPublicacionDeProducto.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //-----------------------------Mostrar------------------------------------//
    public void mostrarPublicacionDeProducto() {
        
            PublicacionDeProductoDAO pdpDAO = new PublicacionDeProductoDAO();
        try {
            for(PublicacionDeProducto e : pdpDAO.MostrarDatosPublicacionDeProducto()){
                System.out.println("DATOS PUBLICACION");
                System.out.println(e.getIdPublicacionDeProducto()+" "+e.getNombrePDP()+" "+e.getFechaPDP()+" "+e.getDescripcionProducto()+" "+e.getImagenProducto()+" "+e.getUsuario().getIdUsuario()+" "+e.getProducto().getIdProducto());
            }
        } catch (SQLException ex) {
            Logger.getLogger(TriliAppTestPublicacionDeProducto.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
