/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triliapptest;

import co.com.triliapp.dao.RolDAO;
import co.com.triliapp.dto.Rol;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author yasuly
 */
public class TriliAppTestRol {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        TriliAppTestRol triliAppRol = new TriliAppTestRol();
        
        triliAppRol.insertarRol();
//        triliAppRol.eliminarRol();
//        triliAppRol.modificarRol();
//        triliAppRol.mostrarRol();
    }
    
    //----------------Insertar
    
    public void insertarRol(){
        
     Rol rolI = new Rol();
     
     rolI.setNombreRL("Invitado");
    
     RolDAO rolDao = new RolDAO();
     try {
         rolDao.insertarRol(rolI);
         System.out.println("Rol Insetado"+ rolI.getIdRol());
     }catch (SQLException ex){
         Logger.getLogger(TriliAppTestRol.class.getName()).log(Level.SEVERE, null, ex);
     }
    }
    
    //------------------Mostar
    
    public void mostrarRol(){
    
        RolDAO rolDao = new RolDAO();
        
        try {
        
            for (Rol e : rolDao.MostrarRol()) {
                System.out.println(e.getIdRol() + " " + e.getNombreRL());
            }
        } catch (SQLException ex) {
            Logger.getLogger(TriliAppTestRol.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    //-------------Eliminar
    
    public void eliminarRol(){
        Rol rolE = new Rol();
        rolE.setIdRol(1);
        RolDAO rolDao = new RolDAO();
        Rol dt;

        try {
            dt = rolDao.eliminarRol(rolE);
            System.out.println("Rol Eliminado");
        } catch (SQLException ex) {
            Logger.getLogger(TriliAppTestRol.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    //-------------Modificar
    
    public void modificarRol(){
        Rol rolA = new Rol();
        
        rolA.setIdRol(6);
        rolA.setNombreRL("nuevonombre");
        
       RolDAO rolDao = new RolDAO();
       try{
           rolDao.modificarRoL(rolA);
           System.out.println("Rol Modificado");
       }catch (SQLException ex){
           Logger.getLogger(TriliAppTestRol.class.getName()).log(Level.SEVERE, null, ex);
       }
        
    }
    
}