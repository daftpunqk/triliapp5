package triliapptest;

import co.com.triliapp.dto.PublicacionDeProducto;
import co.com.triliapp.dto.Usuario;
//----------------------------------------------------------------------------//
import co.com.triliapp.dto.CalificacionPublicacionProducto;
import co.com.triliapp.dao.CalificacionPublicacionProductoDAO;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TriliAppTestCalificacionPublicacionProducto {

    public static void main(String[] args) {

        TriliAppTestCalificacionPublicacionProducto triliAppCPP = new TriliAppTestCalificacionPublicacionProducto();

        triliAppCPP.insertarCalificacionPublicacionProducto();
//        triliAppCPP.eliminarCalificacionPublicacionProducto();
//        triliAppCPP.modificarCalificacionPublicacionProducto();
//        triliAppCPP.mostrarCalificacionPublicacionProducto();
    }

    //--------------------------------------Insertar--------------------------//
    public void insertarCalificacionPublicacionProducto() {

        Usuario Usu = new Usuario();
        Usu.setIdUsuario(2);

        PublicacionDeProducto Pdp = new PublicacionDeProducto();
        Pdp.setIdPublicacionDeProducto(1);

        CalificacionPublicacionProducto Cpp = new CalificacionPublicacionProducto();

        Cpp.setValorCPP(99);
        Cpp.setUsuario(Usu);
        Cpp.setPublicacionDeProducto(Pdp);

        CalificacionPublicacionProductoDAO cppDAO = new CalificacionPublicacionProductoDAO();
        
        try {
            cppDAO.insertCalificacionPublicacionProducto(Cpp);
            System.out.println("CALIFICACION DE PUBLICACION HECHA" + Cpp.getIdCalificacionPublicacionProducto());
        } catch (SQLException ex) {
            Logger.getLogger(TriliAppTestCalificacionPublicacionProducto.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //--------------------------------Eliminar--------------------------------//
    public void eliminarCalificacionPublicacionProducto() {

        CalificacionPublicacionProducto Cpp = new CalificacionPublicacionProducto();
        Cpp.setIdCalificacionPublicacionProducto(1);

        
            CalificacionPublicacionProductoDAO cppDAO = new CalificacionPublicacionProductoDAO();
            
        try {
            cppDAO.EliminarCalificacionPublicacionProducto(Cpp);
            System.out.println("CALIFICACION DE PUBLICACION ELIMINADA");
        } catch (SQLException ex) {
            Logger.getLogger(TriliAppTestCalificacionPublicacionProducto.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    //----------------------------------------Mostrar-------------------------//
    public void mostrarCalificacionPublicacionProducto() {

       
            CalificacionPublicacionProductoDAO cppDAO = new CalificacionPublicacionProductoDAO();
        try {
            for (CalificacionPublicacionProducto e : cppDAO.MostrarDatosCalificacionPublicacionProducto()) {
                System.out.println("DATOS CALIFICACION DE PUBLICACION");
                System.out.println(e.getIdCalificacionPublicacionProducto() + " " + e.getValorCPP() + " " + e.getFechaCPP());
            }
        } catch (SQLException ex) {
            Logger.getLogger(TriliAppTestCalificacionPublicacionProducto.class.getName()).log(Level.SEVERE, null, ex);
        }
       
    }

    //--------------------------------------------Actualizar------------------//
    public void modificarCalificacionPublicacionProducto() {

        Usuario Usu = new Usuario();
        Usu.setIdUsuario(2);

        PublicacionDeProducto Pdp = new PublicacionDeProducto();
        Pdp.setIdPublicacionDeProducto(5);

        CalificacionPublicacionProducto Cpp = new CalificacionPublicacionProducto();

        Cpp.setIdCalificacionPublicacionProducto(2);
        Cpp.setValorCPP(69);
        Cpp.setUsuario(Usu);
        Cpp.setPublicacionDeProducto(Pdp);

        CalificacionPublicacionProductoDAO cppDAO = new CalificacionPublicacionProductoDAO();
        try {
            cppDAO.modificarCalificacionPublicacionProducto(Cpp);
            System.out.println("CALIFICACION DE PUBLICACION MODIFICADA");
        } catch (SQLException ex) {
            Logger.getLogger(TriliAppTestCalificacionPublicacionProducto.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
